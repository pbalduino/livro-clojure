(ns gradual.core
  (:require [clojure.core.typed :as t :refer [ann]]))

(ann soma [Number Number -> Number])

(defn soma [a b]
  (+ a b))

(defn -main [& args]
  (soma 2 3)
  (soma "a" 3)
  (soma true false))