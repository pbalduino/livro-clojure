(defproject gradual "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.7.0-alpha5"]
  					     [org.clojure/core.typed "0.2.77"]]
  :main gradual.core)
