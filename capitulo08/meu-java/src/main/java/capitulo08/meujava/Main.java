package capitulo08.meujava;

import java.util.ArrayList;
import java.util.List;

import clojure.java.api.Clojure;
import clojure.lang.IFn;

import static clojure.java.api.Clojure.var;

public class Main {
  private static final IFn atom    = var("clojure.core", "atom");
  private static final IFn deref   = var("clojure.core", "deref");
  private static final IFn inc     = var("clojure.core", "inc");
  private static final IFn map     = var("clojure.core", "map");
  private static final IFn println = var("clojure.core", "println");
  private static final IFn range   = var("clojure.core", "range");
  private static final IFn reduce  = var("clojure.core", "reduce");
  private static final IFn require = var("clojure.core", "require");
  private static final IFn soma    = var("clojure.core", "+");
  private static final IFn swap    = var("clojure.core", "swap!");
  private static final IFn symbol  = var("clojure.core", "symbol");

  public static void main (String ... args) {
    Main app = new Main();

    // app.var_e_invoke();

    app.require();
  }

  public void var_e_invoke() {
    println.invoke(
      "A soma de 3 e 2 é: ",
      soma.invoke(3, 2));


    Object lista1 = range.invoke(1, 11);
    println.invoke(lista1);

    Object lista2 = map.invoke(inc, lista1);
    println.invoke(lista2);

    Object resultado = reduce.invoke(soma, lista2);
    println.invoke(resultado);

    final Object contador = atom.invoke(0);

    println.invoke("O valor de contador é:", deref.invoke(contador));

    swap.invoke(contador, inc);

    println.invoke("O valor de contador agora é:", deref.invoke(contador));

    (new Thread () {
      public void run() {
        swap.invoke(contador, inc);
        println.invoke("Dentro da thread:", deref.invoke(contador));
      };
    }).start();

    println.invoke("E finalmente o contador agora é:", deref.invoke(contador));
  }

  public void require() {
    require.invoke(symbol.invoke("clojure.pprint"));

    final IFn pprint  = var("clojure.pprint", "pprint");

    Object livro = Clojure.read("{:nome \"Plínio\" :sobrenome \"Balduino\" :titulo \"Clojure\" :editora \"Casa do Código\"}");

    pprint.invoke(livro);
// {:nome "Plínio",
//  :sobrenome "Balduino",
//  :titulo "Clojure",
//  :editora "Casa do Código"}
  }
}