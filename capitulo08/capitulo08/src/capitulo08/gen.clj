(ns capitulo08.gen
  (:gen-class 
    :name "capitulo08.Uia"
    :methods [[^{:static true} hello [] void]
              [^{java.lang.Deprecated true} mofado [] void]]
    :prefix "method-"))

(defn method-hello []
  (println "Olá!"))

(defn method-mofado [this]
  (println "Esse método está velho e mofado"))