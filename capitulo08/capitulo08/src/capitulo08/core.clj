(ns capitulo08.core)

(defn ola-mundo []
  (.println (System/out) "Olá, mundo!"))

(defn -main [& args]
  (ola-mundo))
