package meu.codigo.java;

public class MinhaClasse {
	public static final String UMA_CONSTANTE = "Uma constante";

	public String umMembro = "Esse aqui pode ser alterado";

	private String texto = "Um texto qualquer";

	public static String metodoEstatico() {
		return "Um método estático de classe";
	}

	public String metodoDeInstancia() {
		return "Um método de instancia";
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getTexto() {
		return texto;
	}

	public static long soma(long a, long b) {
		return a + b;
	}

	public static long somaArray(long[] vetor) {
		long soma = 0;
		for (int i = 0; i < vetor.length; i++) {
			soma += vetor[i];
		}
		return soma;
	}
}