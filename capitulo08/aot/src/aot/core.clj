(ns aot.core
  (:gen-class
    :name         aot.MeuNumero
    :extends      java.lang.Number
    :implements   (clojure.lang.IDeref)
    :qualquercoisa true
    :constructors {[String] []
                   [Long]   []}
    :init         init
    :state        value))

(defn -main [& args]
  (println "Vá brincar no REPL"))

(defn -init [value]
  (if (= (type value) java.lang.String)
    [[] (Long/parseLong value 16)]
    [[] value]))

(defn -toString [this]
  (Long/toHexString (.value this)))

(defn -longValue [this]
  (.value this))

(defn -deref [this]
  (.value this))