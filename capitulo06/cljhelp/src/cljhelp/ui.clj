(ns cljhelp.ui)

(defn- clear-screen []
	(doseq [x (range 100)]
		(println)))

(defn menu []
	(clear-screen)
	(println "Menu principal")
	(println "==============")
	(println "[1] Pesquisar ")
	(println "[3] Sair      "))

(defn search []
	(clear-screen)
	(println "Pesquisar")
	(println "==============")
	(print "Operador: "))

(defn result [x]
	(clear-screen)
	(println x))