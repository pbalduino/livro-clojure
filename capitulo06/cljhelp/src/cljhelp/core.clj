(ns cljhelp.core
	(:require [cljhelp.ns-data :as data]
		        [cljhelp.db      :as db]
		        [cljhelp.ui      :as ui]))

(defn- load-data []
	(doseq [v (data/extract-doc)]
		(db/insert! v)))

(defn- search []
	(ui/search)
	(let [operator (read-line)
		    doc      (db/search operator)]
		(ui/result doc)
		(read-line)))

(defn- main-menu []
  (loop []
    (ui/menu)
    (let [op (read-line)]
      (case op
        "1" (search)
        "3" (println "Até mais")
        :else (println "Opção inválida"))
      (when-not (= op "3")
        (recur)))))

(defn -main [& args]
  (load-data)
  (main-menu))