(ns cljhelp.db
	(:refer-clojure :exclude [count]))

(def ^:private data (atom {}))

(defn insert! [{id :id doc :doc}]
	(swap! data assoc id doc))

(defn count []
	(clojure.core/count @data))

(defn search [item]
	(@data (symbol item)))