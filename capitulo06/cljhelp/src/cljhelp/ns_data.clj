(ns cljhelp.ns-data)

(defn extract-doc []
	(let [vars (ns-publics 'clojure.core)
		    ks   (keys vars)]
		(for [k ks]
			{:id  k,
       :doc (:doc (meta (k vars)))})))