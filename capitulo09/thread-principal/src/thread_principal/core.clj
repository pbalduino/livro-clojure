(ns thread-principal.core
  (:gen-class))

(def global 0)

(defn- ola-thread []
  (let [thread-name (.getName (Thread/currentThread))]
    (println "Thread atual:" thread-name)
    (println "Dormindo:" thread-name)
    (def global (+ (.length thread-name) global))
    (Thread/sleep 2000)
    (println "Acordei:" thread-name)))

(defn -main [& args]
  (.start (Thread. ola-thread "outra"))
  (ola-thread)
  (println global))