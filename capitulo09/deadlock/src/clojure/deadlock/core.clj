(ns deadlock.core
  (:import (deadlock Conta))
  (:gen-class))

(def conta-roberta (Conta. 200.0))
(def conta-helena (Conta. 200.0))
(def lock)

(defn- transferir [de para valor]
  (locking de
    (.sacar de valor))
  (locking para
    (.depositar para valor)))

(defn -main [& args]
  (println "Roberta comecou com" (.getSaldo conta-roberta))
  (println "Helena  comecou com" (.getSaldo conta-helena))

  (dotimes [n 100]
    (.start (Thread. #(transferir conta-roberta conta-helena 50)))
    (.start (Thread. #(transferir conta-helena conta-roberta 50))))

  (Thread/sleep 5000)

  (println "Roberta terminou com" (.getSaldo conta-roberta))
  (println "Helena  terminou com" (.getSaldo conta-helena))
  (println))