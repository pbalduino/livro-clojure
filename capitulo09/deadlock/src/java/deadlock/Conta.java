package deadlock;

public class Conta {
    private double saldo;

    public Conta(double valor) {
        this.saldo = valor;
    }

    public void depositar(double valor) throws InterruptedException {
        this.saldo += valor;
        Thread.sleep(10);
    }

    public void sacar(double valor) throws InterruptedException {
        this.saldo -= valor;
        Thread.sleep(10);
    }

    public double getSaldo() {
        return this.saldo;
    }
}