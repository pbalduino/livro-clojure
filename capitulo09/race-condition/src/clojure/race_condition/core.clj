(ns race-condition.core
  (:import [race_condition Sum])
  (:gen-class))

(def global (Sum. 0))

(defn- race-condition []
  (dotimes [n 20]
    (locking global
      (.add global 1))))

(defn -main [& args]
  (dotimes [n 100]
    (.start (Thread. race-condition)))
  (Thread/sleep 500)
  (println "Resultado: " (.getValue global))
  (println))