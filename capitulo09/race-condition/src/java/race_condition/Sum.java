package race_condition;

public class Sum {
    private int value;

    public Sum(int value) {
        this.value = value;
    }

    public void add(int value) {
        this.value += value;
    }

    public int getValue() {
        return value;
    }
}