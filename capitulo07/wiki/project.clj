(defproject wiki "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0-alpha2"]
                 [org.apache.commons/commons-compress "1.8"]
                 [org.clojure/data.xml "0.0.8"]]
  :main wiki.core
  :aot  [wiki.core])
