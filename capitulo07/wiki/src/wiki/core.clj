(ns wiki.core
  (:require [clojure.java.io :as io]
            [clojure.data.xml :as xml]
            [clojure.pprint :as pp])
  (:import  [org.apache.commons.compress.compressors.bzip2 BZip2CompressorInputStream])
  (:gen-class))

(defn articles [item]
  (let [tag     (:tag item)
        content (:content item)
        nspace  (->> content
                     (filter #(= (:tag %) :ns))
                     first
                     :content
                     first)]
    (and (= tag :page)
         (= nspace "0"))))

(defn xml-reader [filename]
  (-> filename 
      io/file 
      io/input-stream 
      (BZip2CompressorInputStream. true)
      io/reader
      xml/parse
      :content))

(defn -main [& args]
  (let [file     (first args)
        xml-seq  (->> file
                      xml-reader
                      (filter articles))]
    (println (count xml-seq))))