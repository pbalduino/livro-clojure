(ns capitulo03.core)

(defn texto []
  (println "Texto original"))

(defn -main [& args]
  (loop []
    (texto)
    (Thread/sleep 2000)
    (recur)))